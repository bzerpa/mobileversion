import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { HttpClient } from '@angular/common/http';
import { Platform } from 'ionic-angular';


@Injectable()
export class AppConfigurationService {

    private baseUrl:string = "/fedeApi";
       
    constructor(public httpClient: HttpClient, public platform: Platform) {
        if(this.platform.is("cordova")){
          this.baseUrl="https://bedelias.sytes.net:1001/ProyectoServicioRest";
        }
      }
    getLoginServiceUrl():string {
        return this.baseUrl + "/login" 
    }

    getUserServiceUrl():string {
        return this.baseUrl + "/api/v1//Usuarios" 
    }
    
    getCarreraServiceUrl():string{
        return this.baseUrl + "/api/v1/Carrera"
    }

    getCarrerasCursadasServiceUrl():string{
        return this.baseUrl + "/api/v1/Carrera/obtenerMisInscripcionesCarrera";
    }
    getCursoServiceUrl():string{
        return this.baseUrl + "/api/v1/Curso"
    }

    getAsignaturaServiceUrl():string{
        return this.baseUrl + "/api/v1/Asignatura"
    }
    
    getCursosInscribibles(idCarrera:number):string {
        return this.baseUrl + `/api/v1/Carrera/${idCarrera}/obtenerCursosParaInscribirme`;
    }

    getMisCursosUrl(idCarrera:number):string{
        return this.baseUrl + `/api/v1/Carrera/${idCarrera}/obtenerMisInscripcionesCursos`;
    }

    getInscripcionACursoUrl(idCurso:number):string{
        return this.baseUrl + `/api/v1/Curso/${idCurso}/inscripcion`;
    }

    getBajaInscripcionCursoUrl(idCurso: number):string{
        return this.baseUrl + `/api/v1/Curso/${idCurso}/inscripcion`;
    }
    

    getExamenesInscribiblesUrl(idCarrera: number): string{
        return this.baseUrl + `/api/v1/Carrera/${idCarrera}/obtenerExamenesParaInscribirme`
    }

    getMisExamenesUrl(idCarrera:number):string{
        return this.baseUrl + `/api/v1/Carrera/${idCarrera}/obtenerMisInscripcionesExamenes`;
    }

    getInscripcionExamenUrl(idExamen:number): string {
        return this.baseUrl + `/api/v1/Examen/${idExamen}/inscripcion`;
    }

    getBajaInscripcionExamenUrl(idExamen:number){

       return this.baseUrl + `/api/v1/Examen/${idExamen}/inscripcion`;
    }

    getCalificacionesCursosUrl(idCarrera: number){
        return this.baseUrl + `/api/v1/Carrera/${idCarrera}/calificacionesInstanciaEvaluacion`;
    }

    getCalificacionesExamenesUrl(idCarrera: number){
        return this.baseUrl + `/api/v1/Carrera/${idCarrera}/calificacionesInstanciaEvaluacion`;
    }

}