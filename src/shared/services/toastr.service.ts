import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

@Injectable()
export class ToastrService {
    
    
    constructor(private toastCtrl: ToastController) {}

    toastInscripcionPendiente(){
        let toast = this.toastCtrl.create({
            message: 'Inscripción agregada. Pendiente de confirmación.',
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }

    toastAgregadoExitoso(){
        let toast = this.toastCtrl.create({
            message: 'Operación exitosa.',
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }
    
    toastCancelar(){
        let toast = this.toastCtrl.create({
          message: 'Operación cancelada.',
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      }

    toastBajaExitosa(){
        let toast = this.toastCtrl.create({
            message: 'Se ha dado de baja correctamente.',
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }

    toastError(error){
        let toast = this.toastCtrl.create({
            message: error.error.errorMessage,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }
}
