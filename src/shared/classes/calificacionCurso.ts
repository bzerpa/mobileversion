export class CalificacionCurso{
    evaluacion:string;
    nombre:string;
    nota:string;
    tipoInstancia:string;

    constructor(evaluacion:string, nombre:string, nota:string, tipoInstancia:string){
        this.evaluacion = evaluacion,
        this.nombre = nombre,
        this.nota = nota
        this.tipoInstancia = tipoInstancia;
    }
}