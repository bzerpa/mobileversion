export interface IUser {
    id: number;
    email:string;
    name:string;
    token:string;
}

export class User implements IUser{

    constructor(option:any = {}){
        this.id = option.id || 0;
        this.name = option.name || '';
        this.email = option.email || '';
        this.token = option.token || '';
    }
    
    id: number;
    email: string;
    name: string;   
    token: string;
}