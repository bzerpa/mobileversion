export class Carrera{
    id: number;
    nombre:string;
    asignaturas: Array<any>;

    constructor(id:number, nombre:string){
        this.id = id,
        this.nombre = nombre
    }
}

