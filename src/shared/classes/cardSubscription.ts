import { Carrera } from "./carrera";

export class CardSubscription {
    title:string;
    subTitle:string;
    subscribed: boolean;
    carrera: Carrera;
    item: any;
    idInscripcion: number;
}