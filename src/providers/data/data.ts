import { HttpClient  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConfigurationService } from '../../shared/services/app.configuration.service';
import { Carrera } from '../../shared/classes/carrera';

@Injectable()
export class DataProvider {

    carreras = [];
    carrerasCursadas = [];
    carreraSeleccionada: Carrera; 
    
  constructor(public httpClient: HttpClient, public config: AppConfigurationService) {
      this.initCarreraSeleccionada();
  }
  initCarreraSeleccionada(){
    if(this.carrerasCursadas.length>0)
      this.carreraSeleccionada.id = this.carrerasCursadas[0].carrera.id;
    else
      this.carreraSeleccionada = new Carrera(-1, "")
  }

  hayCarreraSeleccionada():boolean{
    return this.carreraSeleccionada.id!=-1;
  }
  cursaMasDeUnaCarrera():boolean{
    return this.carrerasCursadas.length>1;
  }

  cursaCarrera():boolean{
    return this.carrerasCursadas.length>0;
  }

}