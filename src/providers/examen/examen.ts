import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConfigurationService } from '../../shared/services/app.configuration.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ExamenProvider {

  constructor(public http: HttpClient, public config: AppConfigurationService) {

  }

  getExamenesInscribibles(idExamen: number): Observable<any>{
    return this.http.get(this.config.getExamenesInscribiblesUrl(idExamen), { headers: new HttpHeaders().set('Authorization', window.localStorage.getItem('token'))})
    .catch(this.handleError);
  }

  getMisExamenes(idCarrera: number): Observable<any> {
    return this.http.get(this.config.getMisExamenesUrl(idCarrera), { headers: new HttpHeaders().set('Authorization', window.localStorage.getItem('token'))})
    .catch(this.handleError);
  }

  private handleError(err: HttpErrorResponse) {
    return Observable.throw(err);
  }

  inscribirmeAExamen(idExamen){

    const options = {
      headers: new HttpHeaders({
        'Authorization': window.localStorage.getItem('token'),
        'Content-Type': 'application/json; charset=utf-8'
      })};

      return this.http.post(this.config.getInscripcionExamenUrl(idExamen), "", options)
      .catch(this.handleError);
  }

  bajaExamen(idExamen: number){

    const options = {
      headers: new HttpHeaders({
        'Authorization': window.localStorage.getItem('token'),
        'Content-Type': 'application/json; charset=utf-8'
      })};
      return this.http.delete(this.config.getBajaInscripcionExamenUrl(idExamen), options)
      .catch(this.handleError);
  }
}
