import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AppConfigurationService } from '../../shared/services/app.configuration.service';

@Injectable()
export class CalificacionProvider {

  constructor(public http: HttpClient, public config: AppConfigurationService) {

  }

  getCalificacionesCurso(idCarrera: number): Observable<any>{
    return this.http.get(this.config.getCalificacionesCursosUrl(idCarrera), { headers: new HttpHeaders().set('Authorization', window.localStorage.getItem('token'))})
    .catch(this.handleError);
  }

  getCalificacionesExamen(idCarrera: number): Observable<any>{
    return this.http.get(this.config.getCalificacionesExamenesUrl(idCarrera), { headers: new HttpHeaders().set('Authorization', window.localStorage.getItem('token'))})
    .catch(this.handleError);
  }

  private handleError(err: HttpErrorResponse) {
    return Observable.throw(err);
  }
}
