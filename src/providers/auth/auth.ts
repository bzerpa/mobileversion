import { Injectable } from '@angular/core';
import { IUser } from '../../shared/classes/user';
import { Observable } from 'rxjs/Observable';
import { Platform } from 'ionic-angular';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

@Injectable()
export class AuthProvider {
  baseUrl =  "/fedeApi";
  currentUser: IUser;
  redirectUrl: string;

  constructor(public httpClient: HttpClient, public platform: Platform) {
    if(this.platform.is("cordova")){
      this.baseUrl="https://bedelias.sytes.net:1001/ProyectoServicioRest";
    }
  }

  setCurrentUser(user){
    this.currentUser = user;
  }

  login(userName:string, password:string): Observable<any> {
    let url:string =  (this.baseUrl + '/loginUsuarioEstudiante?loginUser=' + userName + '&loginPassword=' + password);
    return this.httpClient.post(url, {})
      .catch(this.handleError);
  }

  private handleError(err: HttpErrorResponse) {
    return Observable.throw(err);
  }

  logout(): void {
    this.currentUser = null;
  }

  isLoggedIn(): boolean {
    return !!window.localStorage.getItem('token');
  }

  getToken(){
    if(this.isLoggedIn()){
      return this.currentUser.token;
    }
  }
}
