import { Injectable } from '@angular/core';
import { AppConfigurationService } from '../../shared/services/app.configuration.service';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CarreraProvider {

  constructor(public httpClient: HttpClient, public config: AppConfigurationService) {
  }

  //Retorno las carreras disponibles.
  getCarreras(token:string): Observable<any>{
      const options = {
        headers: new HttpHeaders({
          'authorization': window.localStorage.getItem('token'),
         'content-Type': 'application/json; charset=utf-8'
        })};
      return this.httpClient.get(this.config.getCarreraServiceUrl(), options)
    .catch(this.handleError);
  }

  getCarrerasCursadas(token:string): Observable<any>{
    const options = {
      headers: new HttpHeaders({
        'authorization': window.localStorage.getItem('token'),
       'content-Type': 'application/json; charset=utf-8'
      })};
      return this.httpClient.get(this.config.getCarrerasCursadasServiceUrl(),
      options
      )
      .catch(this.handleError);
  }

  //El usuario se inscribe a una carrera
  realizarInscripcion(idCarrera:string, token:string): Observable<any> {
    
    const options = {
      headers: new HttpHeaders({
        'Authorization': token,
        'Content-Type': 'application/json; charset=utf-8'
      })};
    return this.httpClient.post(`${this.config.getCarreraServiceUrl()}/${idCarrera}/inscribirme`, "", options)

    .catch(this.handleError)
  } 

  //Handleo el error
  private handleError(err: HttpErrorResponse) {
    return Observable.throw(err);
  }

}
