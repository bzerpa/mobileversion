import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AppConfigurationService } from '../../shared/services/app.configuration.service';

@Injectable()
export class CursoProvider {

  constructor(public httpClient: HttpClient, public config: AppConfigurationService) {

  }

  getCursosInscribibles(idCarrera: number): Observable<any>{
    return this.httpClient.get(this.config.getCursosInscribibles(idCarrera), { headers: new HttpHeaders().set('Authorization', window.localStorage.getItem('token'))})
    .catch(this.handleError);
  }

  getMisCursos(idCarrera: number): Observable<any>{
    return this.httpClient.get(this.config.getMisCursosUrl(idCarrera), { headers: new HttpHeaders().set('Authorization', window.localStorage.getItem('token'))})
    .catch(this.handleError);
  }

  private handleError(err: HttpErrorResponse) { 
    return Observable.throw(err);
  }

  inscribirmeACurso(token: string, idCurso: number): Observable<any>{

    const options = {
      headers: new HttpHeaders({
        'Authorization': token,
        'Content-Type': 'application/json; charset=utf-8'
      })};

    return this.httpClient.post(this.config.getInscripcionACursoUrl(idCurso), "", options)
    .catch(this.handleError);
  }

  bajaCurso(token: string, idCurso: number): Observable<any>{

    const options = {
      headers: new HttpHeaders({
        'Authorization': token,
        'Content-Type': 'application/json; charset=utf-8'
      })};

      return this.httpClient.delete(this.config.getBajaInscripcionCursoUrl(idCurso), options)
  }

  /*
  
  */
}
