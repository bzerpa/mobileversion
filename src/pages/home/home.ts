import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from '../../pages/login/login';
import { FCM } from '@ionic-native/fcm';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { DataProvider } from '../../providers/data/data';
import { CarreraProvider } from '../../providers/carrera/carrera';
import { Observable } from 'rxjs/Observable';
import { ToastrService } from '../../shared/services/toastr.service';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  userName:string;
  constructor(private carreraProv: CarreraProvider, private toastCtrl: ToastrService,  public dataProv: DataProvider, private fcm: FCM, public navCtrl: NavController, private auth: AuthProvider, private platform: Platform, public screenOrientation: ScreenOrientation) {
    
    this.platform.ready().then(() => {
      this.fcm.onNotification().subscribe(data => {
              if (data.wasTapped){
                  navCtrl.push(TabsPage);
                }
              else{
                navCtrl.push(TabsPage);
              } 
          },
          error => this.toastCtrl.toastError(error)
      );
      this.initUserData();
      this.initDataCarrera();
    });
  }

  logout(){
    window.localStorage.clear();
    this.auth.logout();
    this.navCtrl.setRoot(LoginPage);
  }

  initUserData(){
    var user = JSON.parse(window.localStorage.getItem('currentUser'))
    this.userName = user.nombre
  }

  initDataCarrera(){
    const url1 = this.carreraProv.getCarreras(window.localStorage.getItem('token'));
    const url2 = this.carreraProv.getCarrerasCursadas(window.localStorage.getItem('token'))
        
    Observable.forkJoin([url1, url2]).subscribe(results =>{
      this.dataProv.carrerasCursadas = [];
      this.dataProv.carreras = results[0].data;     
      results[1].data.forEach(item => {
        if(item.aprobado){
          this.dataProv.carrerasCursadas.push(item)
        }
      })
    })

  }


}
