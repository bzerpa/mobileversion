import { Component } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { ToastrService } from '../../shared/services/toastr.service';
import { DataProvider } from '../../providers/data/data';
import { ExamenProvider } from '../../providers/examen/examen';
import { CardSubscription } from '../../shared/classes/cardSubscription';
import { Observable } from 'rxjs/Observable';
import { FCM } from '@ionic-native/fcm';

@Component({
  selector: 'examen',
  templateUrl: 'examen.html'
})
export class ExamenPage {

  filteredList: CardSubscription[] = [];
  filtroCarrera: string;
  filtroExamen: string;
  examenes = [];
  misExamenes = [];
  items = [];

  constructor(private fcm: FCM, private alertCtrl: AlertController, private toastCtrl: ToastrService, private dataProv: DataProvider, private examenProvider: ExamenProvider) {
    
    this.dataProv.initCarreraSeleccionada();
    this.getExamanesInscribibles();
    this.initializeItems();
    this.initFiltros();
  }

  getExamanesInscribibles():boolean{
    if(this.filtroCarrera == "Carrera"){
      return false;
    }
    else{
      this.filteredList = [];    
      const url1 = this.examenProvider.getExamenesInscribibles(this.dataProv.carreraSeleccionada.id);
      const url2 = this.examenProvider.getMisExamenes(this.dataProv.carreraSeleccionada.id);
      Observable.forkJoin([url1, url2]).subscribe(results => {
        results[0].data.forEach(item => {
          let card = new CardSubscription();
          card.title = item.asignatura.nombre;

          card.item = item;
          let inscripcion = results[1].data.find(x => x.instanciaEvaluacion.asignatura.id == item.asignatura.id);
          if (inscripcion){
            card.subTitle = "Inscripto";
            card.subscribed = true;
            card.idInscripcion = inscripcion.id;
          } else {
            card.subTitle = "No Inscripto";
            card.subscribed = false;
          }
          this.filteredList.push(card);
        });

        this.items=this.filteredList;
      })
      return true;
    }
    }
  

  getMisExamenes(){
    this.filteredList = [];
    this.examenProvider.getMisExamenes(this.dataProv.carreraSeleccionada.id).subscribe(
      (result)=>{
        result.data.forEach(item => {
          let card = new CardSubscription();
          card.title = item.instanciaEvaluacion.asignatura.nombre;
          card.item = item.instanciaEvaluacion;
          card.subscribed = true;
          card.idInscripcion = item.id;
          this.filteredList.push(card);
        })
        this.items= this.filteredList;
      },
        (error)=>{ this.toastCtrl.toastError(error) })

  }

  inscribirAExamen(examen){
    this.examenProvider.inscribirmeAExamen(examen.item.id).subscribe(
      (result)=>{
        if(result.status=="OK"){
          examen.subscribed = true;
          this.toastCtrl.toastAgregadoExitoso();
          this.fcm.subscribeToTopic((result.data.instanciaEvaluacion.topicoNotificacion));
          if(this.filtroExamen == "Ya inscripto"){
            this.getMisExamenes();
          }
          else{
            this.getExamanesInscribibles();

          }
        }
      },
      (error)=>{
        this.toastCtrl.toastError(error)}
    );
  }

  getInfo(examen){
    status;
    var date= new Date(examen.item.fechaHora);
    var time:string = JSON.stringify(examen.item.fechaHora);
    if(examen.subscribed){
      status = "Ya estás inscripto al examen."
    }else{
      status = "No estás inscripto."
    }
    let alert = this.alertCtrl.create({
      title: `${examen.title}`,
      message: `<br/>Fecha: ${date.toLocaleDateString()}<br/>Hora: ${time.split("T")[1].substring(0, 5)}<br/>${status}`,
      buttons: ['OK']
    });
    alert.present();
  }

  bajaInscripcionExamen(examen){
    var topico = (examen.item.topicoNotificacion);
    this.examenProvider.bajaExamen(examen.idInscripcion).subscribe(
      (result)=>{
        if(result.status=="OK"){
          examen.subscribed = false;
          this.fcm.unsubscribeFromTopic(topico);
          this.toastCtrl.toastBajaExitosa();   
          if(this.filtroExamen == "Inscribibles"){
            this.items = this.filteredList
          }
          else{
            this.getMisExamenes();
          }       
        }
      },
      (error)=>{
        this.toastCtrl.toastError(error)
      }
    )
  }

  confirmarAltaInscripcion(examen){
    var date= new Date(examen.item.fechaHora);
    var time:string = JSON.stringify(examen.item.fechaHora);
    let alert = this.alertCtrl.create({
      title: `Confirmar Inscripción`,
      message: `Examen: ${examen.title}<br/>Fecha: ${date.toLocaleDateString()}<br/>Hora sec: ${time.split("T")[1].substring(0, 5)}`,
      buttons: [{
        text: 'Aceptar',
        handler: () => {
          this.inscribirAExamen(examen);
        }
      },
      {
        text: 'Cancelar',
        handler: () => {
          this.toastCtrl.toastCancelar();
        }
      }
      ],
    });
    alert.present();
  }

  confirmarBajaInscripcion(examen){
    let alert = this.alertCtrl.create({
      title: `${examen.title}`,
      message: `Desea cancelar su inscripción al examen?`,
      buttons: [{
        text: 'Aceptar',
        handler: () => {
          this.bajaInscripcionExamen(examen);
        }
      },
      {
        text: 'Cancelar',
        handler: () => {
          this.toastCtrl.toastCancelar();
        }
      }
      ],
    });
    alert.present();
  }

  initializeItems() {
    if(this.filtroExamen=="Inscribibles"){
      this.items = this.filteredList
    }
    else{
      this.items = this.filteredList
    }
  }

  getItems(ev: any) {
    this.initializeItems();
    const val = ev.target.value;
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  filtrarCarreras(){
    let alert = this.alertCtrl.create();
    alert.setTitle('Selecciona una carrera');
    
    this.dataProv.carrerasCursadas.map(
      (item)=>{
        alert.addInput({ 
          type: 'radio',
          label: this.searchCarreraById(item.carrera.id, this.dataProv.carreras), 
          value: this.searchCarreraById(item.carrera.id, this.dataProv.carreras),
          checked: false
        });
      }
    );

    
        alert.addButton({
          text: 'Aceptar',
          handler: data => { 
            this.filtroCarrera = data,
            this.dataProv.carreraSeleccionada.id = this.searchIdByname(data, this.dataProv.carreras),
            this.filtroExamen = 'Inscribibles';
            this.getExamanesInscribibles();
          }
          
        });
        alert.addButton('Cancelar');
        alert.present();
  }

  searchCarreraById(nameKey, myArray) {
    for(var i=0; i<myArray.length; i++){
      if(myArray[i].id === nameKey){
        return myArray[i].nombre;
      }
    }
    return null;
  }

  searchIdByname(nameKey,myArray){
    for(var i=0; i<myArray.length; i++){
      if(myArray[i].nombre === nameKey){
        return myArray[i].id;
      }
    }
    return null;
  }
  filtrarExamenes(){
    let alert = this.alertCtrl.create();
        alert.setTitle('Que tipo de examen desea ver?');
  
        alert.addInput({
          type: 'radio',
          label: 'Inscribibles',
          value: 'Inscribibles',
          checked: false
        });
  
        alert.addInput({
          type: 'radio',
          label: 'Ya inscripto',
          value: 'Ya inscripto',
          checked: false
        });
  
        alert.addButton('Cancel');
        alert.addButton({
          text: 'OK',
          handler: data => {
            if(data=="Inscribibles"){
              this.getExamanesInscribibles();
              
            }
            else if(data=="Ya inscripto"){
              this.getMisExamenes();
            }

            this.filtroExamen = data;
          }
          
        });
        alert.present();
  }


  initFiltros(){
    this.filtroCarrera = this.searchCarreraById(this.dataProv.carrerasCursadas[0].carrera.id, this.dataProv.carreras);
    this.filtroExamen = 'Inscribibles';
  }

  hayResultados():boolean{
    return this.items.length>0;
  }
}
