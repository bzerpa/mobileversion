import { Component } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { ToastrService } from '../../shared/services/toastr.service';
import { CursoProvider } from '../../providers/curso/curso';
import { DataProvider } from '../../providers/data/data';
import { Observable } from 'rxjs/Observable';
import { CardSubscription } from '../../shared/classes/cardSubscription';
import { FCM } from '@ionic-native/fcm';

@Component({
  selector: 'curso',
  templateUrl: 'curso.html'
})
export class CursoPage {
  
  filteredList: CardSubscription[] = [];
  items = [];
  cursos = [];
  misCursos = [];
  filtroCarrera:string;
  filtroCurso:string;

  constructor(private fcm: FCM, private alertCtrl: AlertController, private toastCtrl: ToastrService,  private cursoProvider: CursoProvider, public dataProv: DataProvider){
    
    this.dataProv.initCarreraSeleccionada();
    this.initFiltros();
    this.getCursosInscribibles();
    this.initializeItems();
  }

  getCursosInscribibles():boolean{
    if(this.filtroCarrera == "Carrera"){
      return false;
    }
    else{
      this.filteredList = [];
      const url1 = this.cursoProvider.getCursosInscribibles(this.dataProv.carreraSeleccionada.id);
      const url2 = this.cursoProvider.getMisCursos(this.dataProv.carreraSeleccionada.id); 
      
      Observable.forkJoin([url1, url2]).subscribe(results => {
        results[0].data.forEach(item => {
          let card = new CardSubscription();
          card.title = item.asignatura.nombre;
          card.item = item;
          let inscripcion = results[1].data.find(x => x.instanciaEvaluacion.asignatura.id == item.asignatura.id);
          if (inscripcion){
            card.subTitle = "Inscripto";
            card.subscribed = true;
            card.idInscripcion = inscripcion.id
          } else {
            card.subTitle = "No Inscripto";
            card.subscribed = false;
          }
          this.filteredList.push(card);
        });

        this.items=this.filteredList
      })
    }
  }

  getMisCursos() {
    this.filteredList = [];
    this.cursoProvider.getMisCursos(this.dataProv.carreraSeleccionada.id).subscribe(
      (result)=>{
        result.data.forEach(item => {
          let card = new CardSubscription();
          card.title = item.instanciaEvaluacion.asignatura.nombre;
          card.item = item.instanciaEvaluacion;
          card.idInscripcion = item.id;
          card.subscribed = true;
          this.filteredList.push(card);
        }
      )
        this.items=  this.filteredList;
      },
        (error)=>{ this.toastCtrl.toastError(error) })
  }

  inscribirACurso(curso){
    this.cursoProvider.inscribirmeACurso(window.localStorage.getItem('token'), curso.item.id).subscribe(
      (result)=>{

        if(result.status=="OK"){
          curso.subscribed = true;
          this.toastCtrl.toastAgregadoExitoso();
          this.getCursosInscribibles();  

          this.fcm.subscribeToTopic((result.data.instanciaEvaluacion.topicoNotificacion));
        } 
      },
      (error)=>{
        this.toastCtrl.toastError(error)}
    );
  }

  getInfo(curso){
    if(curso.subscribed){
      status = "Ya estás inscripto al examen."
    }else{
      status = "No estás inscripto."
    }
    let alert = this.alertCtrl.create({
      title: `${curso.title}`,
      message: `Fecha de Inicio: ${curso.item.fechaInicioDictado}<br/>Fecha de Finalización: ${curso.item.fechaFinDictado}<br/>${status}`,
      buttons: ['OK']
    });
    alert.present();
  }

  bajaInscripcionCurso(curso){
    var topico = (curso.item.topicoNotificacion);
    this.cursoProvider.bajaCurso(window.localStorage.getItem('token'), curso.idInscripcion).subscribe(
      (result)=>{ 
        if(result.status=="OK"){
          curso.subscribed = false;
          this.fcm.unsubscribeFromTopic(topico);
          this.toastCtrl.toastBajaExitosa();
          if(this.filtroCurso == "cursando"){
            this.getMisCursos();
          }
          else{
            this.getCursosInscribibles();
          }
        }
      },
      (error)=>{this.toastCtrl.toastError(error)}
    );
  }

  confirmarAltaInscripcion(curso){
    let alert = this.alertCtrl.create({
      title: `Confirmar Inscripción`,
      message: `Curso: ${curso.title}<br/>Fecha de Inicio: ${curso.item.fechaInicioDictado}<br/>Fecha de Finalización: ${curso.item.fechaFinDictado}`,
      buttons: [{
        text: 'Aceptar',
        handler: () => {
          this.inscribirACurso(curso);
        }
      },
      {
        text: 'Cancelar',
        handler: () => {
          this.toastCtrl.toastCancelar();
        }
      }
      ],
    });
    alert.present();
  }

  confirmarBajaInscripcion(curso){
    let alert = this.alertCtrl.create({
      title: `${curso.title}`,
      message: `Desea cancelar su inscripción?`,
      buttons: [{
        text: 'Aceptar',
        handler: () => {
          this.bajaInscripcionCurso(curso);
        }
      },
      {
        text: 'Cancelar',
        handler: () => {
          this.toastCtrl.toastCancelar();
        }
      }
      ],
    });
    alert.present();
  }

  initializeItems() {
    if(this.filtroCurso=="inscribibles"){
      this.items = this.filteredList
    }
    else{
      this.items = this.filteredList      
    }
  }

  getItems(ev: any) {
    this.initializeItems();
    const val = ev.target.value;
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  filtrarCursos(){
    let alert = this.alertCtrl.create();
        alert.setTitle('Que tipo de curso desea ver?');
  

        alert.addInput({
          type: 'radio',
          label: 'Cursando',
          value: 'cursando',
          checked: false
        });
  
        alert.addInput({
          type: 'radio',
          label: 'Inscribibles',
          value: 'inscribibles',
          checked: false
        });

  

        alert.addButton({
          text: 'Aceptar',
          handler: data => {
            if(data=="inscribibles"){
              this.getCursosInscribibles();
            }
            else if(data=="cursando"){
              this.getMisCursos();
            }
            this.filtroCurso = data;            
          }
        });
        alert.addButton('Cancelar');
        alert.present();
  }
  searchCarreraById(nameKey, myArray) {
    for(var i=0; i<myArray.length; i++){
      if(myArray[i].id === nameKey){
        return myArray[i].nombre;
      }
    }
    return null;
  }

  searchIdByname(nameKey,myArray){
    for(var i=0; i<myArray.length; i++){
      if(myArray[i].nombre === nameKey){
        return myArray[i].id;
      }
    }
    return null;
  }

  filtrarCarreras(){
    let alert = this.alertCtrl.create();
    alert.setTitle('Selecciona una carrera');
    
    this.dataProv.carrerasCursadas.map(
      (item)=>{
        alert.addInput({ 
          type: 'radio',
          label: this.searchCarreraById(item.carrera.id, this.dataProv.carreras), 
          value: this.searchCarreraById(item.carrera.id, this.dataProv.carreras),
          checked: false
        });
      }
    );


        alert.addButton({
          text: 'Aceptar',
          handler: data => { 
            this.filtroCarrera = data,
            this.dataProv.carreraSeleccionada.id = this.searchIdByname(data, this.dataProv.carreras),
            this.filtroCurso = "Inscribibles";
            this.getCursosInscribibles();
          }
          
        });
        alert.addButton('Cancelar');
        alert.present();
  }

  initFiltros(){
    this.filtroCurso = "Inscribibles";
    this.filtroCarrera = this.searchCarreraById(this.dataProv.carrerasCursadas[0].carrera.id, this.dataProv.carreras);
  }

  hayResultados():boolean{
    return this.items.length>0;
  }
}

