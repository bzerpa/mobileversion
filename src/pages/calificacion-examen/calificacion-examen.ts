import { Component } from '@angular/core';
import { CalificacionProvider } from '../../providers/calificacion/calificacion';
import { DataProvider } from '../../providers/data/data';
import { ToastrService } from '../../shared/services/toastr.service';

@Component({
  selector: 'calificacion-examen',
  templateUrl: 'calificacion-examen.html'
})
export class CalificacionExamenPage {
  calificacionExamen = [];
  constructor(private toastCtrl: ToastrService, public califaProvider: CalificacionProvider, public dataProvider: DataProvider) {
    this.getCalificacionExamen();
  }

  getCalificacionExamen(){

    this.califaProvider.getCalificacionesExamen(this.dataProvider.carreraSeleccionada.id).subscribe(
      (result)=>{
        for(var i=0; i<result.data.length; i++){
          // this.calificacionExamen[i].nombre= result.data[i].instanciaEvaluacion.asignatura.nombre,
          // this.calificacionExamen[i].fecha = result.data[i].fechaHora,
          // this.calificacionExamen[i].nota = result.data[i].nota
        } 
      },
      (error)=>{ this.toastCtrl.toastError(error) }
    )

    // this.calificacionExamen = [
    //     { nombre: "Examen 1", fecha: "2017/7", nota:"Aprobado 11"},
    //     { nombre: "Examen 2", fecha: "2017/2", nota:"No Aprobado 2"},
    //     { nombre: "Examen 3", fecha: "2014/2", nota:"Aprobado 9"},
    //     { nombre: "Examen 4", fecha: "2016/7", nota:"Aprobado 7"},
    //     { nombre: "Examen 5", fecha: "2015/12", nota:"Aprobado 7"},
    //     { nombre: "Examen 6", fecha: "2015/12", nota:"Aprobado 7"},
    //     { nombre: "Examen 7", fecha: "2016/7", nota:"Aprobado 7"}
    // ]
  }

  tablestyle='bootstrap';


}