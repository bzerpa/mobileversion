import { Component, ViewChild } from '@angular/core';
import { CalificacionCursoPage } from '../calificacion-curso/calificacion-curso';
import { CalificacionExamenPage } from '../calificacion-examen/calificacion-examen';
import { Slides, AlertController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { ToastrService } from '../../shared/services/toastr.service';
import { CalificacionCurso } from '../../shared/classes/calificacionCurso';
import { CalificacionProvider } from '../../providers/calificacion/calificacion';


@Component({
    selector: 'tabs',
    templateUrl: 'tabs.html'
  })
  export class TabsPage {
    @ViewChild('slider') slider: Slides
    page = 0;
    tab1Root = CalificacionCursoPage;
    tab2Root = CalificacionExamenPage;
    filtroCarrera: string;
    filtroCalificacion: string;
    items = [];
    calificacion: CalificacionCurso[] = [];

    constructor(private toastCtrl: ToastrService, private dataProv: DataProvider, private alertCtrl: AlertController, public califaProvider: CalificacionProvider, public dataProvider: DataProvider){
      this.dataProv.initCarreraSeleccionada();
      this.initFiltros();
      this.getCalificacionCurso();
      this.initFiltrosCalificaciones();
    }
  
    selectedTab(index) {
      this.slider.slideTo(index)
    }

    filtrarCarreras(){
      let alert = this.alertCtrl.create();
      alert.setTitle('Selecciona una carrera');
      
      this.dataProv.carrerasCursadas.map(
        (item)=>{
          alert.addInput({ 
            type: 'radio',
            label: this.searchCarreraById(item.carrera.id, this.dataProv.carreras), 
            value: this.searchCarreraById(item.carrera.id, this.dataProv.carreras),
            checked: false
          });
        }
      );

      alert.addButton('Cancel');
        alert.addButton({
          text: 'OK',
          handler: data => { 
            this.filtroCarrera = data,
            this.dataProv.carreraSeleccionada.id = this.searchIdByname(data, this.dataProv.carreras),
            this.getCalificacionCurso();
          }
          
        });
        alert.present();
    } 

    searchIdByname(nameKey,myArray){
      for(var i=0; i<myArray.length; i++){
        if(myArray[i].nombre === nameKey){
          return myArray[i].id;
        }
      }
      return null;
    }
    //PRUEBAS

    toggleSection(i){
      this.items[i].open = !this.items[i].open
    }

    toggleItem(i, j){
      this.items[i].children[j].open = !this.items[i].children[j].open;
    }

    searchCarreraById(nameKey, myArray) {
      for(var i=0; i<myArray.length; i++){
        if(myArray[i].id === nameKey){
          return myArray[i].nombre;
        }
      }
      return null;
    }
   
    initFiltros(){
      this.filtroCarrera = this.searchCarreraById(this.dataProv.carrerasCursadas[0].carrera.id, this.dataProv.carreras);
    }

    getCalificacionCurso(){

      this.califaProvider.getCalificacionesCurso(this.dataProvider.carreraSeleccionada.id).subscribe(
        (result)=>{

          for(var i=0; i<result.data.length; i++){          
            this.calificacion[i] = new CalificacionCurso(result.data[i].estadoEvaluacion, result.data[i].instanciaEvaluacion.asignatura.nombre, result.data[i].nota, result.data[i].instanciaEvaluacion.tipoInstancia) ;
          }

            this.items=this.calificacion
            this.aplicarFiltroCalificacionCurso();

        },
        (error)=>{ this.toastCtrl.toastError(error) }
      )
    }

    hayResultados():boolean{
      return this.items.length>0;
    }

    aplicarFiltroCalificacionCurso(){
      var prov = this.calificacion.filter(calif => calif.tipoInstancia=="CURSO")
      this.items = prov;
    }

    aplicarFiltroCalificacionExamen(){
      var prov = this.calificacion.filter(calif => calif.tipoInstancia=="EXAMEN")
      this.items = prov;
    }


    filtrarCalificaciones(){

      let alert = this.alertCtrl.create();
          alert.setTitle('Qué tipo de calificacion desea ver?');
    
  
          alert.addInput({
            type: 'radio',
            label: 'Cursos',
            value: 'cursos',
            checked: false
          });
    
          alert.addInput({
            type: 'radio',
            label: 'Exámenes',
            value: 'examenes',
            checked: false
          });
  
    
  
          alert.addButton({
            text: 'Aceptar',
            handler: data => {
              if(data=="cursos"){
                this.aplicarFiltroCalificacionCurso();
              }
              else if(data=="examenes"){
                this.aplicarFiltroCalificacionExamen();
              }
              this.filtroCalificacion = data;            
            }
          });
          alert.addButton('Cancelar');
          alert.present();
    }

    initFiltrosCalificaciones(){
      this.filtroCalificacion = "Cursos";
      this.aplicarFiltroCalificacionCurso();
    }
}

 