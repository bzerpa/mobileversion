import { Component } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { ToastrService } from '../../shared/services/toastr.service';
import { CarreraProvider } from '../../providers/carrera/carrera';
import { DataProvider } from '../../providers/data/data';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'carrera',
  templateUrl: 'carrera.html'
})
export class CarreraPage {
  items: any;
  filteredList = [];
  carrerasCursadas= [];
  constructor(private alertCtrl: AlertController, private toastCtrl: ToastrService, private carreraProv: CarreraProvider, public dataProv: DataProvider) {
    this.initCarreras();
    this.initializeItems();
  }

  initializeItems() {
    this.items = this.filteredList;
  }
  getInfo(carrera){
    let alert = this.alertCtrl.create({
      title: `${carrera.nombre}`,
      message: `Descripción: ${carrera.descripcion}<br/><br/>Estado: ${carrera.subTitle}`,
      buttons: ['OK']
    });
    alert.present();
  }

  inscripcionCarrera(carrera){

    this.carreraProv.realizarInscripcion(carrera.id, window.localStorage.getItem('token')).subscribe(
      (result)=>{
        if(result.status == "OK"){
          this.initCarreras();
          this.toastCtrl.toastInscripcionPendiente();  
        }
      },
      (error)=>{ this.toastCtrl.toastError(error)})
  }

  confirmarAlta(carrera){
    let alert = this.alertCtrl.create({
      title: `Confirmar Inscripción`,
      message: `Carrera: ${carrera.nombre}`,
      buttons: [{
        text: 'Aceptar',
        handler: () => {this.inscripcionCarrera(carrera)}
      },
      {
        text: 'Cancelar',
        handler: () => {this.toastCtrl.toastCancelar()}
      }],
    });
    alert.present();
  }

  getItems(ev: any) {
    this.initializeItems();
    const val = ev.target.value;
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  initCarreras(){
    this.filteredList = [];
        const url1 = this.carreraProv.getCarreras(window.localStorage.getItem('token'));
        const url2 = this.carreraProv.getCarrerasCursadas(window.localStorage.getItem('token'))  
        Observable.forkJoin([url1, url2]).subscribe(
          (results) => {       
            this.dataProv.carrerasCursadas = [];
            this.dataProv.carreras = results[0].data;           
            results[0].data.forEach(item => {
              let card:any;
              card = item;
              let inscripcion = results[1].data.find(x => x.carrera.id == item.id);
              if (inscripcion && inscripcion.aprobado){
                card.subTitle = "Ya estas Inscripto";
                card.subscribed = true;
              } else if(inscripcion && !inscripcion.aprobado){
                card.subTitle = "Tienes una inscripcion pendiente de Aprobacion";
                card.subscribed = true;
              }else {
                card.subTitle = "No Inscripto";
                card.subscribed = false;
              }
              this.filteredList.push(card);
            });
            results[1].data.forEach(item => {
              if(item.aprobado){
                this.dataProv.carrerasCursadas.push(item)
              }
            })
            this.items=this.filteredList
          },
        (error)=>{this.toastCtrl.toastError(error)})
  }

  hayResultados():boolean{
    return this.items.length>0;
  }
}