import { Component } from '@angular/core';
import { DataProvider } from '../../providers/data/data';
import { ToastrService } from '../../shared/services/toastr.service';
import { CalificacionProvider } from '../../providers/calificacion/calificacion';
import { CalificacionCurso } from '../../shared/classes/calificacionCurso';

@Component({
  selector: 'calificacion-curso',
  templateUrl: 'calificacion-curso.html'
})
export class CalificacionCursoPage {
 
  calificacionCurso: CalificacionCurso[] = [];
  constructor(private toastCtrl: ToastrService, public califaProvider: CalificacionProvider, public dataProvider: DataProvider) {
    this.getCalificacionCurso();
  }

  getCalificacionCurso(){

    this.califaProvider.getCalificacionesCurso(this.dataProvider.carreraSeleccionada.id).subscribe(
      (result)=>{
       // this.calificacionCurso[0]= new CalificacionCurso("A", "B", "C")
        for(var i=0; i<result.data.length; i++){
         // this.calificacionCurso[i] = new CalificacionCurso(result.data[i].estadoEvaluacion, result.data[i].instanciaEvaluacion.asignatura.nombre, result.data[i].nota) ;

        } 
      },
      (error)=>{ this.toastCtrl.toastError(error) }
    )
  
  }

}