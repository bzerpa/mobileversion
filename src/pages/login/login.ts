import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HomePage } from '../home/home';
import { DataProvider } from '../../providers/data/data';
import { ToastrService } from '../../shared/services/toastr.service';
import { OlvideContrasenaPage } from '../olvide-contrasena/olvide-contrasena';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  formLogin: FormGroup;
    constructor(private toastCtrl: ToastrService,
      public navCtrl: NavController, 
    private formBuilder: FormBuilder,
    public navParams: NavParams, 
    private auth: AuthProvider, public dataProv: DataProvider, public alertCtrl: AlertController) {
      
      this.formLogin = this.formBuilder.group({
        username:[''], password:['']
      })

  }

  login(FormLogin){
    let userName:string = FormLogin.value.username;
    let password:string = FormLogin.value.password;
    this.auth.login(userName, password).subscribe(
      (result) =>{        
          let user = {
              id: '', 
              name: userName, 
              token: JSON.stringify(result.data[0])
          };

        window.localStorage.setItem('token', result.data[0]);
        window.localStorage.setItem('currentUser', JSON.stringify(result.data[1]));
        this.auth.setCurrentUser(user);
        this.navCtrl.setRoot(HomePage);
      },
      (error)=>{
        if(error.status == 403){
          let alert = this.alertCtrl.create({
            title: 'Error de Autenticación',
            message: error.error.errorMessage,
            buttons: [{
              text:'OK',
            handler:()=>{
              this.formLogin = this.formBuilder.group({
                username:[''], password:['']
              })
            }
          }]
          });
          alert.present();
        }
        else{
          this.toastCtrl.toastError(error)
          
        }
      }
      );   
  }
  
  ionViewDidLoad() {

  }

  recuperarPassword(){
    this.navCtrl.push(OlvideContrasenaPage);
  }
}