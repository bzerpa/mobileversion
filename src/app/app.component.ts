import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, NavController } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CarreraPage } from '../pages/carrera/carrera';
import { CursoPage } from '../pages/curso/curso';
import { HomePage } from '../pages/home/home';
import { ExamenPage } from '../pages/examen/examen';
import { TabsPage } from '../pages/tabs/tabs';
import { AuthProvider } from '../providers/auth/auth';
import { LoginPage } from '../pages/login/login';
import { timer } from 'rxjs/observable/timer';
import { FCM } from '@ionic-native/fcm';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = LoginPage;
  showSplash = true;
  pages: Array<{title: string, component: any}>;
  constructor(private auth: AuthProvider, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public menu: MenuController, private fcm: FCM) {
    if(this.auth.isLoggedIn() === true){
      this.rootPage = HomePage;
    }else {
      this.rootPage = LoginPage;
    }
    this.initializeApp();
    
    this.pages = [
      {title: 'Inscribirme a Carrera', component: CarreraPage},
      {title: 'Inscribirme a Curso', component: CursoPage},
      {title: 'Inscribirme a Examen', component: ExamenPage},
      {title: 'Mis Calificaciones', component: TabsPage},
    
    ]
  }

  initializeApp(){
      this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      timer(3000).subscribe(()=>this.showSplash = false)
    });
  }

  openPage(page) {
    this.menu.close();
    this.nav.setRoot(page.component);
  }
  goHome(){
    this.menu.close();
    this.nav.setRoot(HomePage);
  }

}