import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { CarreraPage } from '../pages/carrera/carrera';
import { CursoPage } from '../pages/curso/curso';
import { ExamenPage } from '../pages/examen/examen';
import { TabsPage } from '../pages/tabs/tabs';
import { CalificacionCursoPage } from '../pages/calificacion-curso/calificacion-curso';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CalificacionExamenPage } from '../pages/calificacion-examen/calificacion-examen';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { ToastrService } from '../shared/services/toastr.service';
import { AuthProvider } from '../providers/auth/auth';
import { LoginPage } from '../pages/login/login';
import { HttpModule } from '@angular/http';
import { CarreraProvider } from '../providers/carrera/carrera';
import { AppConfigurationService } from '../shared/services/app.configuration.service';
import { HttpClientModule } from '@angular/common/http';
import { CursoProvider } from '../providers/curso/curso';
import { FCM } from '@ionic-native/fcm';
import { DataProvider } from '../providers/data/data';
import { ExamenProvider } from '../providers/examen/examen';
import { CalificacionProvider } from '../providers/calificacion/calificacion';
import { OlvideContrasenaPage } from '../pages/olvide-contrasena/olvide-contrasena';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CarreraPage,
    CursoPage,
    ExamenPage,
    TabsPage,
    CalificacionCursoPage,
    CalificacionExamenPage,
    LoginPage,
    OlvideContrasenaPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    SuperTabsModule.forRoot(),
    NgxDatatableModule,
    HttpModule,
    HttpClientModule,

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CarreraPage,
    CursoPage,
    ExamenPage,
    TabsPage,
    CalificacionCursoPage,
    CalificacionExamenPage,
    LoginPage,
    OlvideContrasenaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ToastrService,
    AuthProvider,
    HttpModule,
    CarreraProvider,
    AppConfigurationService,
    CursoProvider,
    FCM,
    DataProvider,
    ScreenOrientation,
    ExamenProvider,
    CalificacionProvider
  ]
})
export class AppModule {}
